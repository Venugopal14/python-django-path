## What is test automation?

* https://en.wikipedia.org/wiki/Test_automation

* Unit testing:
    - https://en.wikipedia.org/wiki/Unit_testing
    - http://softwaretestingfundamentals.com/unit-testing/
* Integration testing:
    - http://softwaretestingfundamentals.com/integration-testing/
    - https://softwareengineering.stackexchange.com/questions/48237/what-is-an-integration-test-exactly

It's OK if you still don't understand how exactly do tests work. Go through the below resources to get a more practical introduction
____________

## Testing in Python

### Getting Started Testing

#### Talk by Ned Batchelder
Video: https://www.youtube.com/watch?v=FxSsnHeWQBY
Slides: https://speakerdeck.com/pycon2014/getting-started-testing-by-ned-batchelder

### Real Python article

* https://realpython.com/python-testing/

As usual, practice the examples in the talk and articles.

## Mocking

* https://stackoverflow.com/questions/2665812/what-is-mocking
* https://medium.com/@yeraydiazdiaz/what-the-mock-cheatsheet-mocking-in-python-6a71db997832


____________

## Test Driven Development

* https://en.wikipedia.org/wiki/Test-driven_development
* https://medium.freecodecamp.org/learning-to-test-with-python-997ace2d8abe

___________

## Test coverage

* https://en.wikipedia.org/wiki/Code_coverage


 