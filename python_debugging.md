## Debugging Tips


### Resources


* https://blog.hartleybrody.com/debugging-code-beginner/
* https://www.codementor.io/mattgoldspink/how-to-debug-code-efficiently-and-effectively-du107u9jh
* https://jvns.ca/blog/2019/06/23/a-few-debugging-resources/
* http://jonskeet.uk/csharp/debugging.html

### Some important tips from the above resources:

* Avoid debugging large amounts of code. Run your code each time you make a small change
* Always Reproduce the Bug Before You Start Changing Code
* Understand Stack Traces
* Read the error message
* Google the error message.
* Print things a lot
* Start with code that already works
* Comment-out code
* Write a Test Case that Reproduces the Bug
* Know Your Error Codes

________

### Some tips of my own...

* Check if you need to better understand a concept. For example, if you're debugging an issue about forms, learn more about forms before resuming your debugging process.
* Avoid trial and error debugging. There's no point in trying random things hoping it would fix your code.
* Identify the root cause of the issue. Just because a function raised an error, it doesn't mean the bug occurred in that function. Maybe another function had a bug and called this function with invalid data, maybe a column in the database was null, etc.
* Allocate time for learning new things everyday. Having a strong grasp on the tech stack you are working on will help you 1. ship features quickly, 2. spend less time on debugging, 3. improve your confidence
* Learn to use debuggers. For Python, get started with a visual debugger such as VSCode or PyCharm. When debugging front end and JavaScript, use Chrome Dev Tools all day long. - Workshop
* Keep a close eye on Chrome Dev Tools and the console. You'll find issues relating 404s, sessions, JavaScript errors immediately. 
* If one idea doesn't seem promising, jump to a different one.
* Don't be intimidated, be confident and optimistic instead.
* Take a break. Work on some other problem and come to it soon.
_________

> When you have eliminated the impossible, whatever remains, however improbable, must be the truth.

https://philosiblog.com/2012/05/22/when-you-have-eliminated-the-impossible-whatever-remains-however-improbable-must-be-the-truth/

_________________

## Tools:

### Ipython

Tutorial:

* https://ipython.readthedocs.io/en/stable/interactive/tutorial.html

### pdb

#### Tutorials:

* https://www.digitalocean.com/community/tutorials/how-to-use-the-python-debugger

* https://realpython.com/python-debugging-pdb/

#### Cheatsheet

https://appletree.or.kr/quick_reference_cards/Python/Python%20Debugger%20Cheatsheet.pdf


