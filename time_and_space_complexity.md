## Time and Space Complexity


* [Time Complexity Summary (30 minutes) on mycodeschool](https://www.youtube.com/playlist?list=PL2_aWCzGMAwI9HK8YPVBjElbLbI3ufctn) - 4 videos

* [Complexity for recursive programs](https://www.youtube.com/watch?v=ncpTxqK35PI) (8 minutes) on mycodeschool

* [Big-O: How Code Slows as Data Grows - PyCon 2018](https://www.youtube.com/watch?v=duvZ-2UK0fc)

* Understanding time complexity with Python examples - https://towardsdatascience.com/understanding-time-complexity-with-python-examples-2bda6e8158a7


(Optional) Additionally you can refer to the Algorithms and Data Structures course by [Ravindrababu Ravula on youtube](https://www.youtube.com/watch?v=aGjL7YXI31Q&list=PLEbnTDJUr_IeHYw_sfBOJ6gk5pie0yP-0) (First 6 videos cover ): 

Create an account on [Interviewbit](https://www.interviewbit.com/) and solve the practice problems in the Time Complexity section.


Algorithms Visualization:

https://www.cs.usfca.edu/~galles/visualization/Algorithms.html

________

## Session

* Intuitive understanding of time complexity using examples
* Time Complexity of operations on common data structures in Python
* Hash Tables
* Linked List
* Trees

_______

### Exercises

Write pseudocode explaining how you'll solve the problem. Highlight all the datastructures used to solve the problem


#### Sorted items

You need to store a bunch of items that are always sorted. You need to support the following operations, `add` and `delete`

```
Initial values: []

add 1
[1]

add 0

[0, 1]

add 10

[0, 1, 10]

delete 1

[0, 10]

delete 0

[10]
```

#### Unique Visitors to website

How would you count the number of unique visitors to your website using the ip-address as the identity.

#### Autocomplete:

You have a form field where users enter their country. And you'd like to suggest names of countries as users type each character. What data structure would you use?

