## WebScraper

1. Build a web crawler that downloads 100 pages from a static website
2. Store the web pages in a SQL database


Create a design doc which contains:

* All the important modules, classes and functions and their arguments and return values
* The workflow - Which functions will each which other function etc.
* Structure of SQL tables.

Send the design doc to me, get it reviewed and then start coding.

__________


## Part 2

3. Use `networkx` to find the shortest path between two pages, if one exists
4. Also Download all the images in each page.
5. Classify images using an ML API. Tag the images.

__________


## Common Checklist

### 1. Project presentation

* Create a `.gitignore` from `https://github.com/github/gitignore/blob/master/Python.gitignore`
* Have a `requirements.txt`
* Work under a virtualenv
* Create a `README.md`. Must include

    0. What the project does
    1. How to create the virtualenv
    2. pip install -r requirements.txt
    3. How to run the program.
    4. The different arguments supported by the program
* Don't commit unused files

### 2. Modules

Have separate modules for

1. SQL / Database related stuff. This module must provide functions such as `insert_web_page_into_db` which takes `url`, `html_doc`, `status` as arguments and other DB functions. Don't have SQL code in any other module.

2. Crawler - The main application logic must be in this class.

Create other modules if required.


### Coding Standards

* Follow PEP8 guidelines - Install an extension that automatically detects these for you. 

**VSCode** -  https://www.google.com/search?ei=mXSLXJfKM9zaz7sPkYCO6Ac&q=setup+vs+code+for+python&oq=setup+vs+code+for+python&gs_l=psy-ab.3..0j0i22i30.2468.3935..4097...2.0..0.147.1004.7j3......0....1..gws-wiz.......0i71j35i39j0i13j0i13i5i30j0i22i10i30.1KndUp-zoqw

**Sublime** - Install Anaconda and AutoPEP8 packages.

* Naming - Give descriptive names.

Examples

  **𝙭** ----> **✓**

* `insert_to_db ` ------> `insert_web_page_in_db`

* `get_data`  -------> `fetch_all_rows_in_table`


### Functions - Follow the Unix philosophy 

    * Write functions that do one thing and do it well.
    * Write functions that work well together

### Project-specific stuff

* Handle network errors. If there's a network error when making a request, save the appropriate status in the database.
* Don't batch insert web pages into the db. Store web pages in the database as soon as you fetch them.
* Set `url` as the primary key.
* There must be only place in the codebase where you do `requests.get`. 
* Don't hardcode any URLs
____________________________

## Don't stop working when the code works. Ensure the code is modular, readable and efficient.


